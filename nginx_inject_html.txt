# sub_filter 
server {
    listen       80;
    server_name  localhost;

    location / {
        # inject script tag before cloing head tag
        sub_filter </head>
            '<script language="javascript" src="/script2.js"></script>
</head>';
        sub_filter_once on;
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

}
